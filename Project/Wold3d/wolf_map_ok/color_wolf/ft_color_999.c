/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_color_999.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: enaboule <enaboule@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 10:40:10 by enaboule          #+#    #+#             */
/*   Updated: 2016/06/02 11:24:19 by enaboule         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../src/wolf.h"

void	ft_color_put_825(t_wlf *wlf)
{
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 770)
		wlf->clr->b_w = 5;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 775)
		wlf->clr->b_w = 10;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 780)
		wlf->clr->b_w = 15;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 785)
		wlf->clr->b_w = 20;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 790)
		wlf->clr->b_w = 25;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 795)
		wlf->clr->b_w = 30;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 800)
		wlf->clr->b_w = 35;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 805)
		wlf->clr->b_w = 40;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 810)
		wlf->clr->b_w = 45;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 815)
		wlf->clr->b_w = 50;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 820)
		wlf->clr->b_w = 55;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 825)
		wlf->clr->b_w = 60;
	ft_color_put_885(wlf);
}

void	ft_color_put_885(t_wlf *wlf)
{
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 830)
		wlf->clr->b_w = 65;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 835)
		wlf->clr->b_w = 70;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 840)
		wlf->clr->b_w = 75;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 845)
		wlf->clr->b_w = 80;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 850)
		wlf->clr->b_w = 85;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 855)
		wlf->clr->b_w = 90;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 860)
		wlf->clr->b_w = 95;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 865)
		wlf->clr->b_w = 100;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 870)
		wlf->clr->b_w = 105;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 875)
		wlf->clr->b_w = 110;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 880)
		wlf->clr->b_w = 115;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 885)
		wlf->clr->b_w = 120;
	ft_color_put_945(wlf);
}

void	ft_color_put_945(t_wlf *wlf)
{
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 890)
		wlf->clr->b_w = 125;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 895)
		wlf->clr->b_w = 130;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 900)
		wlf->clr->b_w = 135;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 905)
		wlf->clr->b_w = 140;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 910)
		wlf->clr->b_w = 145;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 915)
		wlf->clr->b_w = 150;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 920)
		wlf->clr->b_w = 155;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 925)
		wlf->clr->b_w = 160;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 930)
		wlf->clr->b_w = 165;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 935)
		wlf->clr->b_w = 170;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 940)
		wlf->clr->b_w = 175;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 945)
		wlf->clr->b_w = 180;
	ft_color_put_995(wlf);
}

void	ft_color_put_995(t_wlf *wlf)
{
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 950)
		wlf->clr->b_w = 185;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 955)
		wlf->clr->b_w = 190;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 960)
		wlf->clr->b_w = 195;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 965)
		wlf->clr->b_w = 200;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 970)
		wlf->clr->b_w = 205;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 975)
		wlf->clr->b_w = 210;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 980)
		wlf->clr->b_w = 215;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 985)
		wlf->clr->b_w = 220;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 990)
		wlf->clr->b_w = 225;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 995)
		wlf->clr->b_w = 230;
}
