/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_color_255.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: enaboule <enaboule@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 10:40:10 by enaboule          #+#    #+#             */
/*   Updated: 2016/06/02 10:52:37 by enaboule         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../src/wolf.h"

void	ft_color_put_60(t_wlf *wlf)
{
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 5)
		wlf->clr->r_w = 5;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 10)
		wlf->clr->r_w = 10;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 15)
		wlf->clr->r_w = 15;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 20)
		wlf->clr->r_w = 20;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 25)
		wlf->clr->r_w = 25;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 30)
		wlf->clr->r_w = 30;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 35)
		wlf->clr->r_w = 35;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 40)
		wlf->clr->r_w = 40;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 45)
		wlf->clr->r_w = 45;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 50)
		wlf->clr->r_w = 50;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 55)
		wlf->clr->r_w = 55;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 60)
		wlf->clr->r_w = 60;
	ft_color_put_120(wlf);
}

void	ft_color_put_120(t_wlf *wlf)
{
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 65)
		wlf->clr->r_w = 65;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 70)
		wlf->clr->r_w = 70;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 75)
		wlf->clr->r_w = 75;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 80)
		wlf->clr->r_w = 80;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 85)
		wlf->clr->r_w = 85;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 90)
		wlf->clr->r_w = 90;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 95)
		wlf->clr->r_w = 95;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 100)
		wlf->clr->r_w = 100;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 105)
		wlf->clr->r_w = 105;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 110)
		wlf->clr->r_w = 110;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 115)
		wlf->clr->r_w = 115;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 120)
		wlf->clr->r_w = 120;
	ft_color_put_180(wlf);
}

void	ft_color_put_180(t_wlf *wlf)
{
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 125)
		wlf->clr->r_w = 125;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 130)
		wlf->clr->r_w = 130;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 135)
		wlf->clr->r_w = 135;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 140)
		wlf->clr->r_w = 140;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 145)
		wlf->clr->r_w = 145;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 150)
		wlf->clr->r_w = 150;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 155)
		wlf->clr->r_w = 155;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 160)
		wlf->clr->r_w = 160;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 165)
		wlf->clr->r_w = 165;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 170)
		wlf->clr->r_w = 170;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 175)
		wlf->clr->r_w = 175;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 180)
		wlf->clr->r_w = 180;
	ft_color_put_240(wlf);
}

void	ft_color_put_240(t_wlf *wlf)
{
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 185)
		wlf->clr->r_w = 185;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 190)
		wlf->clr->r_w = 190;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 195)
		wlf->clr->r_w = 195;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 200)
		wlf->clr->r_w = 200;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 205)
		wlf->clr->r_w = 205;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 210)
		wlf->clr->r_w = 210;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 215)
		wlf->clr->r_w = 215;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 220)
		wlf->clr->r_w = 220;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 225)
		wlf->clr->r_w = 225;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 230)
		wlf->clr->r_w = 230;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 235)
		wlf->clr->r_w = 235;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 240)
		wlf->clr->r_w = 240;
	ft_color_put_255(wlf);
}

void	ft_color_put_255(t_wlf *wlf)
{
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 245)
		wlf->clr->r_w = 245;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 250)
		wlf->clr->r_w = 250;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 255)
		wlf->clr->r_w = 255;
}
