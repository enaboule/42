/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_color_765.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: enaboule <enaboule@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 10:40:10 by enaboule          #+#    #+#             */
/*   Updated: 2016/06/02 11:04:44 by enaboule         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../src/wolf.h"

void	ft_color_put_570(t_wlf *wlf)
{
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 515)
		wlf->clr->r_w = 255;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 520)
		wlf->clr->r_w = 250;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 525)
		wlf->clr->r_w = 245;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 530)
		wlf->clr->r_w = 240;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 535)
		wlf->clr->r_w = 235;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 540)
		wlf->clr->r_w = 230;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 545)
		wlf->clr->r_w = 225;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 550)
		wlf->clr->r_w = 220;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 555)
		wlf->clr->r_w = 215;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 560)
		wlf->clr->r_w = 210;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 565)
		wlf->clr->r_w = 205;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 570)
		wlf->clr->r_w = 200;
	ft_color_put_630(wlf);
}

void	ft_color_put_630(t_wlf *wlf)
{
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 575)
		wlf->clr->r_w = 195;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 580)
		wlf->clr->r_w = 190;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 585)
		wlf->clr->r_w = 185;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 590)
		wlf->clr->r_w = 180;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 595)
		wlf->clr->r_w = 175;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 600)
		wlf->clr->r_w = 170;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 605)
		wlf->clr->r_w = 165;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 610)
		wlf->clr->r_w = 160;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 615)
		wlf->clr->r_w = 155;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 620)
		wlf->clr->r_w = 150;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 625)
		wlf->clr->r_w = 145;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 630)
		wlf->clr->r_w = 140;
	ft_color_put_690(wlf);
}

void	ft_color_put_690(t_wlf *wlf)
{
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 635)
		wlf->clr->r_w = 135;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 640)
		wlf->clr->r_w = 130;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 645)
		wlf->clr->r_w = 125;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 650)
		wlf->clr->r_w = 120;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 655)
		wlf->clr->r_w = 115;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 660)
		wlf->clr->r_w = 110;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 665)
		wlf->clr->r_w = 105;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 670)
		wlf->clr->r_w = 100;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 675)
		wlf->clr->r_w = 95;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 680)
		wlf->clr->r_w = 90;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 685)
		wlf->clr->r_w = 85;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 690)
		wlf->clr->r_w = 80;
	ft_color_put_750(wlf);
}

void	ft_color_put_750(t_wlf *wlf)
{
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 695)
		wlf->clr->r_w = 75;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 700)
		wlf->clr->r_w = 70;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 705)
		wlf->clr->r_w = 65;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 710)
		wlf->clr->r_w = 60;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 715)
		wlf->clr->r_w = 55;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 720)
		wlf->clr->r_w = 50;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 725)
		wlf->clr->r_w = 45;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 730)
		wlf->clr->r_w = 40;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 735)
		wlf->clr->r_w = 35;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 740)
		wlf->clr->r_w = 30;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 745)
		wlf->clr->r_w = 25;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 750)
		wlf->clr->r_w = 20;
	ft_color_put_765(wlf);
}

void	ft_color_put_765(t_wlf *wlf)
{
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 755)
		wlf->clr->r_w = 15;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 760)
		wlf->clr->r_w = 10;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 765)
		wlf->clr->r_w = 5;
}
