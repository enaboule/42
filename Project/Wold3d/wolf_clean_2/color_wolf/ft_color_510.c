/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_color_510.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: enaboule <enaboule@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 10:40:10 by enaboule          #+#    #+#             */
/*   Updated: 2016/06/02 10:55:35 by enaboule         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../src/wolf.h"

void	ft_color_put_315(t_wlf *wlf)
{
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 260)
		wlf->clr->g_w = 5;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 265)
		wlf->clr->g_w = 10;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 270)
		wlf->clr->g_w = 15;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 275)
		wlf->clr->g_w = 20;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 280)
		wlf->clr->g_w = 25;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 285)
		wlf->clr->g_w = 30;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 290)
		wlf->clr->g_w = 35;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 295)
		wlf->clr->g_w = 40;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 300)
		wlf->clr->g_w = 45;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 305)
		wlf->clr->g_w = 50;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 310)
		wlf->clr->g_w = 55;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 315)
		wlf->clr->g_w = 60;
	ft_color_put_375(wlf);
}

void	ft_color_put_375(t_wlf *wlf)
{
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 320)
		wlf->clr->g_w = 65;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 325)
		wlf->clr->g_w = 70;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 330)
		wlf->clr->g_w = 75;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 335)
		wlf->clr->g_w = 80;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 340)
		wlf->clr->g_w = 85;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 345)
		wlf->clr->g_w = 90;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 350)
		wlf->clr->g_w = 95;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 355)
		wlf->clr->g_w = 100;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 360)
		wlf->clr->g_w = 105;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 365)
		wlf->clr->g_w = 110;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 370)
		wlf->clr->g_w = 115;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 375)
		wlf->clr->g_w = 120;
	ft_color_put_435(wlf);
}

void	ft_color_put_435(t_wlf *wlf)
{
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 380)
		wlf->clr->g_w = 125;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 385)
		wlf->clr->g_w = 130;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 390)
		wlf->clr->g_w = 135;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 395)
		wlf->clr->g_w = 140;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 400)
		wlf->clr->g_w = 145;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 405)
		wlf->clr->g_w = 150;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 410)
		wlf->clr->g_w = 155;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 415)
		wlf->clr->g_w = 160;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 420)
		wlf->clr->g_w = 165;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 425)
		wlf->clr->g_w = 170;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 430)
		wlf->clr->g_w = 175;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 435)
		wlf->clr->g_w = 180;
	ft_color_put_495(wlf);
}

void	ft_color_put_495(t_wlf *wlf)
{
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 440)
		wlf->clr->g_w = 185;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 445)
		wlf->clr->g_w = 190;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 450)
		wlf->clr->g_w = 195;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 455)
		wlf->clr->g_w = 200;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 460)
		wlf->clr->g_w = 205;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 465)
		wlf->clr->g_w = 210;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 470)
		wlf->clr->g_w = 215;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 475)
		wlf->clr->g_w = 220;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 480)
		wlf->clr->g_w = 225;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 485)
		wlf->clr->g_w = 230;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 490)
		wlf->clr->g_w = 235;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 495)
		wlf->clr->g_w = 240;
	ft_color_put_510(wlf);
}

void	ft_color_put_510(t_wlf *wlf)
{
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 500)
		wlf->clr->g_w = 245;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 505)
		wlf->clr->g_w = 250;
	if (wlf->parse->tab[wlf->map_x][wlf->map_y] == 510)
		wlf->clr->g_w = 255;
}
