/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_lowercase.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: enaboule <enaboule@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/17 18:40:12 by enaboule          #+#    #+#             */
/*   Updated: 2015/10/19 09:54:06 by enaboule         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_str_is_lowercase(char *str)
{
	int		cpt;

	cpt = 0;
	while (str[cpt] != '\0')
	{
		if (str[cpt] < 97 || str[cpt] > 122)
			return (0);
		cpt++;
	}
	return (1);
}
